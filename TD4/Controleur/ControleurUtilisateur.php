<?php
require_once('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('../vue/utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['login'])) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            ControleurUtilisateur::afficherVue('../vue/utilisateur/detail.php', ['utilisateur' => $utilisateur]);
        } else {

        }
    }

    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('../vue/utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire(): void
    {

        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
            $utilisateur->ajouter();
            ControleurUtilisateur::afficherListe();
        } else {
            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');
        }


    }


    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }


}

?>