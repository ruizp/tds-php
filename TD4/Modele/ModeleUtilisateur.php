<?php

require_once 'ConnexionBaseDeDonnees.php';

class ModeleUtilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;

    private ?array $trajetsCommePassager;


    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null) {
            $this->setTrajetsCommePassager($this->recupererTrajetsCommePassager());
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }


    public function getLogin(): string
    {
        return $this->login;
    }


    public function getPrenom(): string
    {
        return $this->prenom;
    }


    // un setter
    public function setNom(string $nom)
    {

        $this->nom = $nom;
    }

    public function setLogin(string $login)
    {

        $login = substr($login, 0, 64);
        $this->login = $login;
    }

    public function setPrenom(string $prenom)
    {

        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        $utilisateur = new ModeleUtilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
        return $utilisateur;
    }

    public static function getUtilisateurs()
    {
        $utilisateurs = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur');


        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateur = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
            $utilisateurs[] = $utilisateur;
        }

        return $utilisateurs;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    public static function recupererUtilisateurParLogin(string $login): ?ModeleUtilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );
        $pdoStatement->execute($values);

        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (!$utilisateurFormatTableau) {
            return null;
        } else {
            return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
    }

    /**
     * @return ModeleUtilisateur[]
     */
    public static function recupererUtilisateurs(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");

        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateur){
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateur);
        }

        return $utilisateurs;
    }


    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM `trajet` WHERE `id` in (SELECT `trajetId` FROM passager where `passagerLogin` = '$this->login');");
        $pdoStatement->execute();

        $trajets = [];

        foreach ($pdoStatement as $trajet) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajet);
        }
        return $trajets;
    }

    public function ajouter(): bool
    {
        try {
            $creerutilisateur = ConnexionBaseDeDonnees::getPdo()->prepare("INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)");
            $values = array(
                "login" => $this->getLogin(),
                "nom" => $this->getNom(),
                "prenom" => $this->getPrenom(),
            );
            $creerutilisateur->execute($values);
        } catch (PDOException) {
            return false;
        }
        return true;
    }

//    public function __toString(): string
//    {
//        return "nom :  {$this->getNom()} , prenom : {$this->getPrenom()}, login : {$this->getLogin()}";
//    }
}

