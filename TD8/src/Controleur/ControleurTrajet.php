<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe(): void
    {
        $Trajets = (new TrajetRepository)->recuperer(); //appel au modèle pour gérer la BD
        ControleurTrajet::afficherVue('vueGenerale.php', ['trajets' => $Trajets, "titre" => "Liste des Trajets", "cheminCorpsVue" => "trajet/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_REQUEST['id'])) {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_REQUEST['id']);
            ControleurTrajet::afficherVue('vueGenerale.php', ['trajet' => $trajet, "titre" => "détail trajet", "cheminCorpsVue" => "trajet/detail.php"]);
        } else {
            ControleurTrajet::afficherErreur("l'id du trajet n'a pas étais défini");
        }

    }

    public static function supprimer()
    {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $rep = (new TrajetRepository())->supprimer($id);
            if ($rep) {
                $trajets = (new TrajetRepository())->recuperer();
                ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "supprimerTrajet", "cheminCorpsVue" => "trajet/trajetSupprime.php", "trajets" => $trajets, "id" => $id]);
            } else {
                ControleurTrajet::afficherErreur("une erreur est survenu lors de la suppression du trajet");
            }
        } else {
            ControleurTrajet::afficherErreur("l'id du trajet n'a pas étais défini");
        }
    }

    public static function afficherFormulaireCreation()
    {
        $listeLogin = [];
        /** @var Utilisateur $utilisateur */
        foreach ((new UtilisateurRepository())->recuperer() as $utilisateur){
            $listeLogin[] = $utilisateur->getLogin();
    }
        ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Formulaire création", "cheminCorpsVue" => "trajet/formulaireCreation.php", "listeLogin" => $listeLogin]);
    }

    public static function afficherFormulaireMiseAJour()
    {
        if (isset($_REQUEST['id'])) {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_REQUEST['id']);
            ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Formulaire MaJ", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php", "trajet" => $trajet]);
        } else {
            ControleurTrajet::afficherErreur("le trajet n'a pas était préciser");
        }
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;

        $trajet = new Trajet($id, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'], new DateTime($tableauDonneesFormulaire['date']), $tableauDonneesFormulaire['prix'], (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']), $tableauDonneesFormulaire['nonFumeur']);
        return $trajet;
    }

    public static function mettreAJour(){
        if (isset($_REQUEST['nonFumeur'])) {
            $fumeur = 1;
        } else {
            $fumeur = 0;
        }

        if (isset($_REQUEST['depart']) && isset($_REQUEST['arrivee']) && isset($_REQUEST['date']) && isset($_REQUEST['prix']) && isset($_REQUEST['conducteurLogin'])) {
            $trajet = ControleurTrajet::construireDepuisFormulaire(array(
                "id" => $_REQUEST['id'],
                "depart" => $_REQUEST['depart'],
                "arrivee" => $_REQUEST['arrivee'],
                "date" => $_REQUEST['date'],
                "prix" => $_REQUEST['prix'],
                "conducteurLogin" => $_REQUEST['conducteurLogin'],
                "nonFumeur" => $fumeur));

            (new TrajetRepository())->mettreAJour($trajet);
            $trajets = (new TrajetRepository())->recuperer();
            ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Maj du trajet", "cheminCorpsVue" => "trajet/trajetMisAJour.php", "trajets" => $trajets]);

        }else{
            ControleurTrajet::afficherErreur("il manque des données pour mettre a jour le trajet.");
        }
    }

    public static function creerDepuisFormulaire()
    {

        if (isset($_REQUEST['nonFumeur'])) {
            $fumeur = 1;
        } else {
            $fumeur = 0;
        }
        if (isset($_REQUEST['depart']) && isset($_REQUEST['arrivee']) && isset($_REQUEST['date']) && isset($_REQUEST['prix']) && isset($_REQUEST['conducteurLogin'])) {
            $trajet = ControleurTrajet::construireDepuisFormulaire(array(
                "depart"=> $_REQUEST['depart'],
                "arrivee" => $_REQUEST['arrivee'],
                "date" => $_REQUEST['date'],
                "prix" => $_REQUEST['prix'],
                "conducteurLogin" => $_REQUEST['conducteurLogin'],
                "nonFumeur" => $fumeur));

            $rep = (new TrajetRepository())->ajouter($trajet);
            if ($rep) {
                $trajets = (new TrajetRepository())->recuperer();
                ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "trajetCree", "cheminCorpsVue" => "trajet/trajetCree.php", "trajets" => $trajets]);
            } else {
                ControleurTrajet::afficherErreur("il y a eu une erreur dans l'ajout a la base de donnée");
            }
        } else {
            ControleurTrajet::afficherErreur("il manque des informations dans le formulaire");
        }
    }



    public static function afficherErreur(string $messageErreur = ""): void
    {

        ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "erreur", "cheminCorpsVue" => "trajet/erreur.php", "messageErreur" => $messageErreur]);
    }


}