<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Utilisateur;

class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = Utilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['login'])) {
            $utilisateur = Utilisateur::recupererUtilisateurParLogin($_GET['login']);
            ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {

        }
    }

    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {

        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            $utilisateur = new Utilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
            $utilisateur->ajouter();
            $utilisateurs = Utilisateur::recupererUtilisateurs();
            ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "utilisateurCree", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        } else {
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }


}

?>