<!DOCTYPE html>
<html>

<body>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $login = htmlspecialchars($utilisateur->getLogin());
    $loginurl = rawurlencode($utilisateur->getLogin());

    echo '<p> Utilisateur de login <a href="/tds-php/TD5/web/controleurFrontal.php?action=afficherDetail&login=' . $loginurl .'">' . $login . '</a></p>';
}
?>

<br>
<h4><a href="/tds-php/TD5/web/controleurFrontal.php?action=afficherFormulaireCreation">Créer utilisateur</a></h4>
</body>
</html>