<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte . "ici saut de ligne <br><br>";


          $utilisateur = [
          'nom' => 'testnom',
          'prenom' => 'testprenom',
          'login' => 'testnomprenom'
          ];

          $utilisateurs = ['utilisateur1', 'utilisateur2', 'utilisateur3', 'utilisateur4', 'utilisateur5', 'utilisateur6'];


        ?>

        <p> Utilisateur <?php echo "$utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]" ?> <br></p>

    <h1>Listes d'utilisateurs :<br></h1>
        <ul>
    <?php
    if (count($utilisateurs) == 0){
        echo "la liste des utilisateurs et vide";
    }else {
        foreach ($utilisateurs as $user) {
            echo "<li> $user </li>";
        }
    }
    ?>
        </ul>
    </body>
</html> 