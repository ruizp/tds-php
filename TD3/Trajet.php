<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;

    private array $passagers = [];



    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );

        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPassagers(): array
    {
        return $this->recupererPassagers();
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public static function recupererTrajetParId(int $id):?Trajet{
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM trajet WHERE id = :id");
        $pdoStatement->execute(['id'=>$id]);

        $trajetFormatTableau = $pdoStatement->fetch();

        if (!$trajetFormatTableau) {
            return null;
        } else {
            return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }
    }


    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT p.passagerLogin FROM utilisateur u INNER JOIN passager p ON u.login = p.passagerLogin WHERE trajetid = :id");

        $pdoStatement->execute(array("id"=> $this->getId()));

        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateur){
            $utilisateurs[] = Utilisateur::recupererUtilisateurParLogin($utilisateur['passagerLogin']);
        }

        return $utilisateurs;
    }

    public function supprimerPassager(string $passagerLogin): bool{
        $pdostatement = ConnexionBaseDeDonnees::getPdo()->prepare("DELETE FROM passager WHERE passagerLogin = :login AND trajetId = :idtrajet");
        $pdostatement->execute(['login' => $passagerLogin, 'idtrajet' => $this->getId()]);

        if($pdostatement->rowCount() == 1) {
            return true;
        }else{
            return false;
        }
    }

    public function ajouter() {
        $creerTrajet = ConnexionBaseDeDonnees::getPdo()->prepare("INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)");

        $fumeur = $this->nonFumeur?1:0;

        $values = array(
            "depart" => $this->getDepart(),
            "arrivee" => $this->getArrivee(),
            "date" => $this->date->format("Y-m-d"),
            "prix" => $this->getPrix(),
            "conducteurLogin" => $this->conducteur->getLogin(),
            "nonFumeur" => $fumeur,
        );
        $creerTrajet->execute($values);

        $this->setId(ConnexionBaseDeDonnees::getPdo()->lastInsertId());
    }
}
