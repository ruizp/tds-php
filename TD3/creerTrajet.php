<?php
require_once 'Trajet.php';

if (isset($_GET['nonFumeur'])){
    $fumeur = 1;
}else{
    $fumeur = 0;
}
if (isset($_GET['depart']) && isset($_GET['arrivee']) && isset($_GET['date']) && isset($_GET['prix']) && isset($_GET['conducteurLogin'])){
    $trajet = new Trajet(null, $_GET['depart'], $_GET['arrivee'], new DateTime($_GET['date']), $_GET['prix'], Utilisateur::recupererUtilisateurParLogin($_GET['conducteurLogin']), $fumeur);
    $trajet->ajouter();

    echo '<script>
alert ("tout s\'est bien passé")
</script>';

}else {
    echo '<script>
alert ("une erreur est survenue" )
</script>';

    require 'formulaireCreationTrajet.html';
}