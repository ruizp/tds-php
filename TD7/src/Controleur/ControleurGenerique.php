<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;


class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference(): void
    {
        ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "formulairePreference", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference()
    {
        if (isset($_GET['controleur_defaut'])){
            PreferenceControleur::enregistrer($_GET['controleur_defaut']);
            ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "preferenceEnregistree", "cheminCorpsVue" => "preferenceEnregistree.php"]);
        }
    }
}