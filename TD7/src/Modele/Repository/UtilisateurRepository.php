<?php

namespace App\Covoiturage\Modele\Repository;


use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

class UtilisateurRepository extends AbstractRepository
{


    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {
        $login = $utilisateur->getLogin();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM `trajet` WHERE `id` in (SELECT `trajetId` FROM passager where `passagerLogin` = '$login');");
        $pdoStatement->execute();

        $trajets = [];

        foreach ($pdoStatement as $trajet) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajet);
        }
        return $trajets;
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        $utilisateur = new Utilisateur($objetFormatTableau[0], $objetFormatTableau[1], $objetFormatTableau[2]);
        return $utilisateur;
    }


    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }
}