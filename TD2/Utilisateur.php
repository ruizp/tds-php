<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string {
        return $this->nom;
    }


    public function getLogin(): string
    {
        return $this->login;
    }


    public function getPrenom(): string
    {
        return $this->prenom;
    }



    // un setter
    public function setNom(string $nom) {

        $this->nom = $nom;
    }

    public function setLogin(string $login){

        $login = substr($login, 0, 64);
        $this->login = $login;
    }

    public function setPrenom(string $prenom){

        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        $utilisateur = new Utilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
        return $utilisateur;
    }

    public static function getUtilisateurs(){
        $utilisateurs = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur');


        foreach ($pdoStatement as $utilisateurFormatTableau){
            $utilisateurs[] = new Utilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);;
        }

        return $utilisateurs;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string {
        return "nom :  $this->nom, prenom : $this->prenom, login : $this->login";
    }
}

