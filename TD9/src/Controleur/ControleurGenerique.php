<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\PreferenceControleur;


class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres

        // Avec le if/else ternaire
        $messagesFlash = MessageFlash::lireTousMessages();
        // Ou de manière équivalent avec l'opérateur "null coalescent"
        // https://www.php.net/manual/fr/migration70.new-features.php#migration70.new-features.null-coalesce-op
        // $messagesFlash = $_REQUEST["messagesFlash"] ?? [];

        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference(): void
    {
        ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "formulairePreference", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference()
    {
        if (isset($_REQUEST['controleur_defaut'])) {
            PreferenceControleur::enregistrer($_REQUEST['controleur_defaut']);
            MessageFlash::ajouter("success", "La préférence de contrôleur est enregistrée !");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe");
        }
    }

    public static function redirectionVersURL(string $url):void {
        header("Location: $url");
        exit();
    }

}