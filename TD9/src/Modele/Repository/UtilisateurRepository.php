<?php

namespace App\Covoiturage\Modele\Repository;


use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

class UtilisateurRepository extends AbstractRepository
{


    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {
        $login = $utilisateur->getLogin();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM `trajet` WHERE `id` in (SELECT `trajetId` FROM passager where `passagerLogin` = '$login');");
        $pdoStatement->execute();

        $trajets = [];

        foreach ($pdoStatement as $trajet) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajet);
        }
        return $trajets;
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        $utilisateur = new Utilisateur($objetFormatTableau[0], $objetFormatTableau[1], $objetFormatTableau[2], $objetFormatTableau[3], $objetFormatTableau[4], $objetFormatTableau[5], $objetFormatTableau[6], $objetFormatTableau[7]);
        return $utilisateur;
    }


    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        $admin = $utilisateur->isEstAdmin()?1:0;
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
            "estAdminTag" => $admin,
            "emailTag" => $utilisateur->getEmail(),
            "emailAValiderTag" => $utilisateur->getEmailAValider(),
            "nonceTag" => $utilisateur->getNonce(),
        );
    }
}